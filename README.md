# KPZ

Куча маленьких і не дуже лабок по C# :
* [Lab 1 - Robots](Lab 1 - Robots) - Алгоритм для конкурсу по битві роботів. Всі права на битву роботів належать Сердюку Павлу Віталійовичу.
* [Lab 2 - Events](Lab 2 - Events) - Лабораторна по івентам. Нічого особливого
* [Lab 3 - LINQ](Lab 3 - Robots) - Лабораторна по можливостям LINQ. (І всьо StreamApi лучше :Р)
* [Lab 4 - Classes](Lab 4 - Classes) - Лабораторна по визначенню продуктивності виклику методу з 1.000.000 класів.
* [Lab 5 - WPF](Lab 5 - WPF) - Лабораторна по Windows Presentation Foundation. (Тупо Windows Form + XAML)
* [Lab 6 - ADO](Lab 6 - ADO) - Лабораторна по роботі з БД + EntityFramework (Code-First, DB-First)
* [Lab 7 - WebAPI](Lab 7 - WebAPI) - Лабораторна по WebApi.
