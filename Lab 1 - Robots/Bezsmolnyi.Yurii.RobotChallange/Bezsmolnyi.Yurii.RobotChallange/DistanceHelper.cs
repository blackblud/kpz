﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Bezsmolnyi.Yurii.RobotChallange
{
    public class DistanceHelper
    {

        public static int CountPlayers(Position StationPosition, IList<Robot.Common.Robot> robots, Map map)
        {
            int count = 0;

            //for (int x = -2; x < 3; x++)
            //{
            //    for (int y = -2; y < 3; y++)
            //    {
            //        Position checkCellBusy = new Position(StationPosition.X + x, StationPosition.Y + y);

            //        if (IsMyPlayer(checkCellBusy, robots) && checkCellBusy != StationPosition)
            //        {
            //            count++;
            //        }
            //    }


            foreach (Robot.Common.Robot robot in robots)
            {
                if (FindDistance(robot.Position, StationPosition) <= 2)
                {
                    count++;
                }
            }

            return count;


        }

        public static int CountStation(Position CheckPoint, Map map)
        {
            int count = 0;
            for (int x = -2; x < 3; x++)
            {
                for (int y = -2; y < 3; y++)
                {
                    Position checkCellStation = new Position(CheckPoint.X+x, CheckPoint.Y + y);

                    if (IsStation(checkCellStation, map) && checkCellStation != CheckPoint)
                    {
                        count++;
                    }


                }
            }
            return count;
        }

        public static bool IsStation(Position point, Map map)
        {
            foreach (var station in map.Stations)
            {
                if (station.Position == point)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsMyPlayer(Position player, IList<Robot.Common.Robot> robots)
        {
            foreach (var players in robots)
            {
                if ((players.Position == player) && (players.OwnerName.Equals("Yurii Bezsmolnyi")))
                {
                    return true;
                }
            }

            return false;
        }


        public static bool IsPlayer(Position player, IList<Robot.Common.Robot> robots)
        {
            foreach (var players in robots)
            {
                if (players.Position == player)
                {
                    return true;
                }
            }

            return false;
        }
        public static int CountMyRobots(Robot.Common.Robot player, IList<Robot.Common.Robot> robots)
        {
            int count = 0;
            foreach (var players in robots)
            {
                if (players.OwnerName.Equals("Yurii Bezsmolnyi") )
                {
                    count++;
                }
            }

            return count;
        }


        //Повертає кількість клітинок між точкою А і B
        public static int FindDistance(Position a, Position b)
        {
            return (int)Math.Sqrt((Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2)));
        }



        //Шукає найближчу станцію
        public static Position iFindNearestStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (CountPlayers(station.Position, robots, map) > 3)
                {
                    continue;
                }
                int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                if (d < minDistance)

                {

                    minDistance = d;
                    nearest = station;

                }
            }
            return nearest == null ? null : nearest.Position;
        }





        //Шукає найближчу вільну станцію
        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)

                    {

                        minDistance = d;
                        nearest = station;

                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }






        //Чи станція вільна?
        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }



        //Чи клітинка вільна?
        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }
    }
}
