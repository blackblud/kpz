using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Dynamic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Bezsmolnyi.Yurii.RobotChallange
{
    public class BezsmolnyiAlgorithm : IRobotAlgorithm
    {
        public BezsmolnyiAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public int RoundCount { get; set; }

        public static Position FindBestPosition(Position StationPosition, IList<Robot.Common.Robot> robots, Map map)
        {
            int numberOfStations = 0;
            int inumberOfStations = 0;
            Position savedBest = new Position(0,0);
            for (int x = -2; x < 3; x++)
            {
                for (int y = -2; y < 3; y++)
                {
                    Position CheckPoint = new Position(StationPosition.X + x, StationPosition.Y + y);

                    inumberOfStations = DistanceHelper.CountStation(CheckPoint, map);

                    if (numberOfStations <= inumberOfStations)
                    {
                        if (!DistanceHelper.IsMyPlayer(CheckPoint, robots))
                        {
                            numberOfStations = inumberOfStations;
                            savedBest.X = CheckPoint.X;
                            savedBest.Y = CheckPoint.Y;
                        }

                    }
                }
            }

            if (numberOfStations < 2)
            {
                return StationPosition;
            }
            else
            {
                return savedBest;
            }





            //for (int x = -4; x < 5; x++)
            //{
            //    for (int y = -4; y < 5; y++)
            //    {
            //        Position CheckPoint new Position(StationPosition.X + x, StationPosition.Y + y);

            //        if (DistanceHelper.IsStation(CheckPoint, map))
            //        {
            //            Position SecondStationPos = new Position(CheckPoint.X, CheckPoint.Y);
            //            Position BestPos = new Position((StationPosition.X + SecondStationPos.X) / 2, (StationPosition.Y + SecondStationPos.Y) / 2);

            //            if (DistanceHelper.IsMyPlayer(BestPos, robots))
            //            {
            //                BestPos.X = BestPos.X + 1;
            //                return BestPos;
            //            }
            //            else
            //            {
            //                return BestPos;
            //            }
            //        }
            //    }
            //}

            return null;
        }


        //Сам алгоритм
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot CurrentRobot = robots[robotToMoveIndex];
            Position CurrentPos = CurrentRobot.Position;
            Position iStationPos = DistanceHelper.iFindNearestStation(CurrentRobot, map, robots);
            Position StationPos = DistanceHelper.FindNearestFreeStation(CurrentRobot, map, robots);
            
            Position NearPosition = new Position(0, 0);



            Position BestPosition = FindBestPosition(iStationPos, robots, map);






            if (CurrentRobot.Energy > 250 && RoundCount < 40 && DistanceHelper.CountMyRobots(CurrentRobot, robots) < 100)
            {
                return new CreateNewRobotCommand();

            }
            else if (RoundCount >= 40)
            {
                return new CollectEnergyCommand();
            }
            else if (CurrentPos == StationPos)
            {
                return new CollectEnergyCommand();
            }
            else if (DistanceHelper.FindDistance(CurrentPos, iStationPos) < 3)
            {
                return new MoveCommand() { NewPosition = StationPos };

                        //}
                    //else
                    //{
                        //return new MoveCommand() { NewPosition = StationPos };
                    //}
            }
            else
            {
                int DistanceToBest = (int)Math.Pow(DistanceHelper.FindDistance(CurrentPos, BestPosition), 2);

                if (CurrentRobot.Energy < DistanceToBest)
                {
                    return new CollectEnergyCommand();
                }
                else
                {
                    return new MoveCommand() { NewPosition = BestPosition };
                }

                //if (CurrentRobot.Energy < DistanceToBest) 
                //{
                //    Position NewPoint = new Position(0, 0);
                //    NewPoint.X = ((BestPosition.X + CurrentPos.X) / 2);
                //    NewPoint.Y = ((BestPosition.Y + CurrentPos.Y) / 2);

                //    return new MoveCommand() { NewPosition = NewPoint };
                //}
                //else
                //{
                //    return new MoveCommand() { NewPosition = BestPosition };
                //}
            }

            return null;



            //if (CurrentRobot.Energy > 200)
            //{
            //    return new CreateNewRobotCommand();
            //}
            //else
            //{
            //    if (CurrectPosition == StationPosition)
            //    {
            //        return new CollectEnergyCommand();
            //    } 
            //    else if (DistanceHelper.FindDistance(CurrectPosition, StationPosition) < 3)
            //    {
            //        if (RoundCount == 1)
            //        {
            //            return new MoveCommand() { NewPosition = StationPosition };
            //        }
            //        else
            //        {
            //            return new CollectEnergyCommand();
            //        }
            //    }
            //    else
            //    {
            //        //int num1 = (int)Math.Pow((double)DistanceHelper.FindDistance(CurrectPosition, StationPosition), 2.0);
            //        //int num2 = CurrentRobot.Energy;
            //        //if (num1 > num2)
            //        //{
            //        //    Position NewPoint = new Position(0,0);
            //        //    NewPoint.X = (CurrectPosition.X + StationPosition.X) / 2;
            //        //    NewPoint.Y = (CurrectPosition.Y + StationPosition.Y) / 2;
            //        //    return new MoveCommand() { NewPosition = NewPoint };
            //        //}
            //        //else
            //        //{
            //        //    return new MoveCommand() { NewPosition = StationPosition };
            //        //}
            //    }
            //}
        }
        public string Author
        {
            get { return "Yurii Bezsmolnyi"; }
        }
    }
}
