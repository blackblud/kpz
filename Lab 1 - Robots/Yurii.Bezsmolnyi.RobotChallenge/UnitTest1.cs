﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Bezsmolnyi.Yurii.RobotChallange;

namespace Yurii.Bezsmolnyi.RobotChallenge
{
    [TestClass]
    public class UnitTest1
    {
        // Перевірка чи правильно рахує кількість станцій навколо точки Target (В діапазоні (-2;2))
        [TestMethod]
        public void TestCountOfStations1()
        {

            Map map = new Map();
            Position stationPos1 = new Position(7, 6);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos1 });
            Position stationPos2 = new Position(13, 6);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos2 });
            Position stationPos3 = new Position(14, 8);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos3 });

            Position target = new Position(10, 10);
            Position final = new Position(12, 8);

            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(5, 5)}};


            Position a = BezsmolnyiAlgorithm.FindBestPosition(target, robots, map);



            Assert.AreEqual(final, a);
        }


        // Перевірка правильності пріорітету точки Target між двома одинаковими позиціями
        [TestMethod]
        public void TestCountOfStations2()
        {

            Map map = new Map();
            Position stationPos0 = new Position(8, 7);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos0 });
            Position stationPos1 = new Position(7, 7);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos1 });
            Position stationPos2 = new Position(7, 13);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos2 });
            Position stationPos3 = new Position(13, 7);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos3 });
            Position stationPos4 = new Position(13, 13);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos4 });
            Position stationPos5 = new Position(12, 13);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos5 });

            Position target = new Position(10, 10);
            Position final = new Position(12, 12);

            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(5, 5)}};


            Position a = BezsmolnyiAlgorithm.FindBestPosition(target, robots, map);



            Assert.AreEqual(final, a);
        }



        // Перевірка поверненя на дефолтні значення, якщо станції не знайдені
        [TestMethod]
        public void TestCountOfStations3()
        {

            Map map = new Map();
            Position stationPos1 = new Position(0, 0);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos1 });
            Position stationPos2 = new Position(25, 25);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos2 });
            Position stationPos3 = new Position(99, 99);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos3 });

            Position target = new Position(10, 10);
            Position final = new Position(10, 10);

            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(5, 5)}};


            Position a = BezsmolnyiAlgorithm.FindBestPosition(target, robots, map);



            Assert.AreEqual(final, a);
        }




        // Чи буде збирати робот енергію, знаходячись на тій же позиції
        [TestMethod]
        public void TestCollectCommand()
        {
            Map map = new Map();
            Position stationPos = new Position(5,5);
            map.Stations.Add(new EnergyStation(){Energy = 100, Position = stationPos});
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(5, 5)}};

            var SuperAlgorithm = new BezsmolnyiAlgorithm();
            var command = SuperAlgorithm.DoStep(robots, 0, map);



            Assert.IsTrue(command is CollectEnergyCommand);
        }



        // Чи створить робот ще одного робота, якщо буде стояти біля позиції станції, але не на ній
        [TestMethod]
        public void TestCreateNewRobotCommand()
        {
            Map map = new Map();
            Position stationPos = new Position(5, 5);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 2000, Position = new Position(4, 4), OwnerName = "Yurii Bezsmolnyi"}};

            var SuperAlgorithm = new BezsmolnyiAlgorithm();
            var command = SuperAlgorithm.DoStep(robots, 0, map);
            

            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        // Чи добереться робот при енергії в 50, та дистанцією від (10,10) до (5,5)
        [TestMethod]
        public void TestMoveCommand()
        {
            Map map = new Map();
            Position stationPos = new Position(5, 5);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 50, Position = new Position(10, 10), OwnerName = "Yurii Bezsmolnyi"}};

            var SuperAlgorithm = new BezsmolnyiAlgorithm();
            var command = SuperAlgorithm.DoStep(robots, 0, map);


            Assert.IsTrue(command is MoveCommand);
        }



        // Вирахувати середину відрізка між двома позиціями
        [TestMethod]
        public void TestFindCenterOfPositions()
        {
            Map map = new Map();
            Position stationPos = new Position(5, 5);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos });

            var currentRobot = new Robot.Common.Robot()
                {Energy = 2000, Position = new Position(50, 50), OwnerName = "Yurii Bezsmolnyi"};

            Position final = new Position(27,27);
            Position center = new Position((currentRobot.Position.X+stationPos.X) / 2,(currentRobot.Position.Y + stationPos.Y) / 2);

         
            Assert.AreEqual(final, center);
        }




        // Перевірка поверненя на дефолтні значення, якщо станції не знайдені
        [TestMethod]
        public void TestCountOfStationsMore()
        {

            Map map = new Map();
            Position stationPos0 = new Position(8, 7);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos0 });
            Position stationPos1 = new Position(7, 7);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos1 });
            Position stationPos2 = new Position(7, 13);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos2 });
            Position stationPos3 = new Position(13, 7);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos3 });
            Position stationPos4 = new Position(13, 13);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos4 });
            Position stationPos5 = new Position(12, 13);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos5 });

            Position stationPos6 = new Position(7, 10);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos6 });
            Position stationPos7 = new Position(13, 10);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos7 });

            Position target = new Position(10, 10);
            Position final = new Position(12, 12);

            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(5, 5)}};


            Position a = BezsmolnyiAlgorithm.FindBestPosition(target, robots, map);



            Assert.AreEqual(final, a);
        }


        // Перевірка поверненя на дефолтні значення, якщо станції не існує
        [TestMethod]
        public void TestCountStationNull()
        {

            Map map = new Map();


            Position target = new Position(10, 10);
            Position final = new Position(10, 10);

            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(5, 5)}};


            Position a = BezsmolnyiAlgorithm.FindBestPosition(target, robots, map);



            Assert.AreEqual(final, a);
        }






        // Перевірка правильності рахунку кількості роботів навколо станції
        [TestMethod]
        public void TestDistancehelperCountPlayer()
        {

            Map map = new Map();
            Position stationPos0 = new Position(10, 10);
            map.Stations.Add(new EnergyStation() {Energy = 100, Position = stationPos0});


            var Robot1 = new Robot.Common.Robot() {Energy = 200, Position = new Position(10, 10)};
            var Robot2 = new Robot.Common.Robot() {Energy = 200, Position = new Position(50, 50)};
            var Robot3 = new Robot.Common.Robot() {Energy = 200, Position = new Position(11, 11)};
            var Robot4 = new Robot.Common.Robot() {Energy = 200, Position = new Position(9, 9)};
            var Robot5 = new Robot.Common.Robot() {Energy = 200, Position = new Position(10, 11)};

            var robots = new List<Robot.Common.Robot>()
                {Robot1, Robot2, Robot3, Robot4, Robot5};

            Position target = new Position(10, 10);

            int final = 4;

            int number = DistanceHelper.CountPlayers(target, robots, map);


            Assert.AreEqual(final, number);


        }



        // Перевірка правильності рахунку кількості роботів навколо станції
        [TestMethod]
        public void TestDistancehelperCountPlayer1()
        {

            Map map = new Map();
            Position stationPos0 = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPos0 });


            var Robot1 = new Robot.Common.Robot() { Energy = 200, Position = new Position(0, 0) };
            var Robot2 = new Robot.Common.Robot() { Energy = 200, Position = new Position(0, 0) };
            var Robot3 = new Robot.Common.Robot() { Energy = 200, Position = new Position(0, 0) };
            var Robot4 = new Robot.Common.Robot() { Energy = 200, Position = new Position(0, 0) };
            var Robot5 = new Robot.Common.Robot() { Energy = 200, Position = new Position(0, 0) };

            var robots = new List<Robot.Common.Robot>()
                {Robot1, Robot2, Robot3, Robot4, Robot5};

            Position target = new Position(10, 10);

            int final = 0;

            int number = 0;
            number = DistanceHelper.CountPlayers(target, robots, map);


            Assert.AreEqual(final, number);


        }
    }
}
