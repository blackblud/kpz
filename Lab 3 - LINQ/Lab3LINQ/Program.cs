﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Robot> robots = new List<Robot>();

            robots.Add(new Robot(0, "admin", "black", 999, 999, new Location("Hello", "World")));
            robots.Add(new Robot(1, "WALL-E", "red", 1, 97, new Location("WestEurope", "Austria")));
            robots.Add(new Robot(2, "EVE", "yellow", 5, 45, new Location("EastEurope", "Croatia")));
            robots.Add(new Robot(3, "S.A.M", "cyan", 8, 32, new Location("NorthEurope", "Sweden")));
            robots.Add(new Robot(4, "R2D2", "pink", 9, 25, new Location("America", "New York")));
            robots.Add(new Robot(5, "BB8", "purpul", 3, 82, new Location("WestEurope", "Ireland")));
            robots.Add(new Robot(6, "Crowby", "black", 10, 1, new Location("WestEurope", "United_Kindom")));
            robots.Add(new Robot(7, "Sparkle", "green", 9, 31, new Location("EastEurope", "Bulgaria")));
            robots.Add(new Robot(8, "01010010", "blue", 4, 61, new Location("WestEurope", "Monaco")));
            robots.Add(new Robot(9, "Ipux", "dark-green", 3, 78, new Location("SouthEurope", "Italy")));
            robots.Add(new Robot(10, "Norbit", "brown", 6, 52, new Location("SouthEurope", "Albania")));
            robots.Add(new Robot(11, "Clank", "gray", 5, 59, new Location("NorthEurope", "Denmark")));

            int menu;

            do
            {
                Console.WriteLine("\nMenu:");
                Console.WriteLine("1. LINQ Select [Name] and [Color]"); 
                Console.WriteLine("2. LINQ Where [Rank] >= [InputRank]");
                Console.WriteLine("3. LINQ on Dictionary");
                Console.WriteLine("4. List to Array by LINQ"); 
                Console.WriteLine("5. Sort() by using IComparable");
                Console.WriteLine("0. Exit");

                Console.WriteLine("Please enter number of menu : ");
                menu = Convert.ToInt32(Console.ReadLine());

                switch (menu)
                {
                    case 1:
                        Console.WriteLine("\nCase 1:");
                        foreach (var x in robots.Select(i => new{ID = i.id, Name = i.name, Color = i.color}))
                        {
                            Console.WriteLine(x);
                        }

                        break;
                    case 2:
                        Console.WriteLine("\nCase 2:");
                        Console.WriteLine("Input min rank of robots:");
                        var minrank = Convert.ToInt32(Console.ReadLine());
                        foreach (var x in robots.Where(i => i.rank >= minrank))
                        {
                            Console.WriteLine(x);
                        }

                        break;
                    case 3:
                        Console.WriteLine("\nCase 3:");

                        //Dictionary<Robot, List<Robot> map = new Dictionary<Robot, List<Robot>>();

                        var map = robots.GroupBy(x => x.name).ToDictionary(x => x.Key, x => x.ToList());

                        foreach (KeyValuePair<string, List<Robot>> kv in map)
                        foreach (var x in kv.Value.Where(i => i.color.Contains("green")))
                        {
                            Console.WriteLine("[" + kv.Key + "] - " + x.color);
                        }


                        break;
                    case 4:
                        Console.WriteLine("\nCase 4:");

                        var array = robots.Select(x => x.name).OrderBy(x => x.Length).ToArray();
                        foreach (var item in array)
                        {
                            Console.WriteLine(item);
                        }

                        break;
                    case 5:
                        Console.WriteLine("\nCase 5:");

                        robots.Sort();

                        foreach (var x in robots)
                        {
                            Console.WriteLine(x);
                        }


                        break;

                    default:
                        Console.WriteLine("\nError - Invalid input.");
                        break;
                }
            } while (menu != 0);

            Console.WriteLine("END#####.");
        }
    }
}
