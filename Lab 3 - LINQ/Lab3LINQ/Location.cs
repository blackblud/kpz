﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3LINQ
{
    class Location
    {
        public string region { get; set; }
        public string country { get; set; }

        public Location(string region, string country)
        {
            this.region = region;
            this.country = country;
        }
    }
}
