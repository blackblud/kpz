﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3LINQ
{
    class Robot : IComparable<Robot>
    {
        public int id { get; set; }
        public string name { get; set; }
        public string color { get; set; }
        public int rank { get; set; }
        public int countOfSuccessLVL { get; set; }
        public Location location { get; set; }

        public Robot(int id, string name, string color, int rank, int countOfSuccessLVL, Location location)
        {
            this.id = id;
            this.name = name;
            this.color = color;
            this.rank = rank;
            this.countOfSuccessLVL = countOfSuccessLVL;
            this.location = location;
        }

        public int CompareTo(Robot other)
        {
            return countOfSuccessLVL.CompareTo(other.countOfSuccessLVL);
        }

        public override string ToString()
        {
            return "["+ id + "] " + name + " - [Color] " + color + ", [Rank] " + rank + ", [Success Levels] " + countOfSuccessLVL + ", [Location]" + location.region + "_" + location.country;
        }
    }
}
