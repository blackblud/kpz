﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Laba5WPF
{
    /// <summary>
    /// Interaction logic for TournamentTable.xaml
    /// </summary>
    public partial class TournamentTable : Window
    {
        public TournamentTable()
        {
            InitializeComponent();

            List<LightBot> robots = new List<LightBot>();

            robots.Add(new LightBot(0, "admin", "black", 999, 999));
            robots.Add(new LightBot(1, "WALL-E", "red", 1, 90));
            robots.Add(new LightBot(2, "EVE", "yellow", 5, 4501));
            robots.Add(new LightBot(3, "S.A.M", "cyan", 8, 320));
            robots.Add(new LightBot(4, "R2D2", "pink", 9, 2550));
            robots.Add(new LightBot(5, "BB8", "purpul", 3, 8442));
            robots.Add(new LightBot(6, "Crowby", "black", 10, 10));
            robots.Add(new LightBot(7, "Sparkle", "green", 9, 3100));
            robots.Add(new LightBot(8, "01010010", "blue", 4, 6100));
            robots.Add(new LightBot(9, "Ipux", "dark-green", 3, 7200));
            robots.Add(new LightBot(10, "Norbit", "brown", 6, 5254));
            robots.Add(new LightBot(11, "Clank", "gray", 5, 5999));

            foreach(LightBot bot in robots)
            {
                dataGridTable.Items.Add(bot);
            }
        }

        private void add_new(object sender, RoutedEventArgs e)
        {
            //int lvls = add_successLVL.Text;
            //if(Int32.Parse(lvls) > 1000)
            //{
            //    //convert to 1k;
            //    //lvls = 
            //}

            dataGridTable.Items.Add(new LightBot(Int32.Parse(add_id.Text), add_name.Text, add_color.Text, Int32.Parse(add_rank.Text), Int32.Parse(add_successLVL.Text)));

            MessageBox.Show("Record of robot was success added.");
            add_id.Text = "";
            add_name.Text = "";
            add_color.Text = "";
            add_rank.Text = "";
            add_successLVL.Text = "";
        }

        private void reset_all(object sender, RoutedEventArgs e)
        {
            add_id.Text = "";
            add_name.Text = "";
            add_color.Text = "";
            add_rank.Text = "";
            add_successLVL.Text = "";
        }

        private void show_cur_user(object sender, RoutedEventArgs e)
        {
            CurrentUser curus = new CurrentUser();
            block_curuser.Children.Remove(curus);
            block_curuser.Children.Add(curus);
        }
    }


}
