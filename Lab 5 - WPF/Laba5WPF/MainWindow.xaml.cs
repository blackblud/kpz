﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Laba5WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            
        }

        private void Button_Login(object sender, RoutedEventArgs e)
        {
            string login = Login_input.Text;
            string pass = Pass_input.Password;

            if (login.Length < 5 || login.Length > 50)
            {
                Login_input.ToolTip = "Wrong login entered.";
                Login_input.Background = Brushes.Red;
            } else if (pass.Length < 3 || pass.Length > 50)
            {
                Login_input.Background = Brushes.Transparent;
                Pass_input.Background = Brushes.Transparent;
                Pass_input.ToolTip = "Wrong password entered.";
                Pass_input.Background = Brushes.Red;
            } else
            {
                if (login == "admin" && pass == "12345")
                {
                    Login_input.ToolTip = "";
                    Login_input.Background = Brushes.Transparent;
                    Pass_input.ToolTip = "";
                    Pass_input.Background = Brushes.Transparent;

                    MessageBox.Show("Welcome, " + login + "!");
                    
                    TournamentTable tourtable = new TournamentTable();
                    tourtable.Show();
                    this.Hide();
                }
                else
                {
                    Login_input.ToolTip = "";
                    Login_input.Background = Brushes.Transparent;
                    Pass_input.ToolTip = "";
                    Pass_input.Background = Brushes.Transparent;

                    MessageBox.Show("User is Not ADMIN!");
                }
            }
        }
    }
}
