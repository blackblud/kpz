﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba5WPF
{
    public class LightBot
    {
        public int id { get; set; }
        public string name { get; set; }
        public string color { get; set; }
        public int rank { get; set; }
        public int countOfSuccessLVL { get; set; }

        public LightBot(int id, string name, string color, int rank, int countOfSuccessLVL)
        {
            this.id = id;
            this.name = name;
            this.color = color;
            this.rank = rank;
            this.countOfSuccessLVL = countOfSuccessLVL;
        }
    }
}
