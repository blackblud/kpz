﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba4_3Classes
{
    class BaseClass
    {
        public virtual int TestMethod()
        {
            return 0;
        }
    }

    class ClassA : BaseClass
    {
        public override int TestMethod()
        {
            return 1;
        }
    }

    class ClassB : ClassA
    {
        public override int TestMethod()
        {
            return 2;
        }
    }

    class ClassC : ClassB
    {
        public override int TestMethod()
        {
            return 3;
        }
    }

    class ClassD : ClassC
    {
        public override int TestMethod()
        {
            //base.TestMethod();
            return 4;
        }
    }

    
    class ClassE
    {
        public int TestMethod()
        {
            return 5;
        }
    }

    class ClassF : TestInterface
    {
        public int TestMethod()
        {
            return 6;
        }
    }

    class ClassG
    {
        private int field = 555;

        public int Field
        {
            get { return field; }
        }


        public int TestMethod()
        {
            return 7;
        }


    }

}
