﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Laba4_3Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();


            var test5 = new ClassG();
            Type type1 = test5.GetType();

            var resultMethod = type1.GetConstructor(Type.EmptyTypes);
            object classGObject = resultMethod.Invoke(new object[] { });

            MethodInfo method = type1.GetMethod("TestMethod");


            ulong maxValue = 532000000;
              
            object[] array = new BaseClass[500000000]; //Max - 532

            foreach (object x in array)
            {

                BaseClass test = new ClassD();                          // Avast - 9.06  |  System - 12.97                      
                //ClassE test = new ClassE();                           // Avast - 5.07  |  System - 9.17
                //ClassF test = new ClassF();                           // Avast - 5.12  |  System - 8.72
                method.Invoke(classGObject, new object[] { });  // Avast - 9.42  |  System - 9.67     (50.000.000)
                                                                        // Avast - Crush  |  System - 1.36.63 (500.000.000)
                //test.TestMethod();
            }


            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);


            Console.ReadKey();
        }
    }
}
