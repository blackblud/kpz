﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Classes
{
    interface Actions
    {
        void Move(int steps);
        void TurnLeft(int steps);
        void TurnRight(int steps);
        void Jump(int steps);
        void Light(int steps);
        void SpecialCommand(int id);
    }
}
