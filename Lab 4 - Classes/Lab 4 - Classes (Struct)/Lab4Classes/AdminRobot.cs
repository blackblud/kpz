﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab4Classes
{
    struct AdminRobot : Actions
    {
        public void Move(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot did " + i + "-th step on move up.");
            }
        }

        public void TurnLeft(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot did " + i + "-th step turn left.");
                if (i % 4 == 0)
                {
                    Console.WriteLine("Robot return's to same position.");
                }
            }
        }

        public void TurnRight(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot did " + i + "-th step turn right.");
                if (i % 4 == 0)
                {
                    Console.WriteLine("Robot return's to same position.");
                }
            }
        }

        public void Jump(int steps)
        {
            if (steps < 2)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot just did jump. That was cool! U can try again.");
            }
            else
            {
                for (int i = 1; i <= steps; i++)
                {
                    if (i < 2)
                    {
                        Thread.Sleep(1000);
                        Console.WriteLine("Robot just did jump. That was cool! U can try again.");
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        Thread.Sleep(1000);
                        Console.WriteLine("Robot did another jump. Its was'nt so cool, like in first time...");
                    }
                }
            }
        }

        public void Light(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                if (i % 2 != 0)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Light is On!");
                }
                else
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Light is Off!");
                }
            }
        }

        public void SpecialCommand(int id)
        {
            Console.WriteLine("Deleting robot on ID - " + id);

            
            object obj = id; //boxing
            int i = (int)obj; //unboxign

            AdminPanel panel = new AdminPanel();
            panel.SetPoints(0, i);
        }

        private class AdminPanel
        {
            public void SetPoints(int amount, int id)
            {
                Console.Write("Points are removed from robot with ID - " + id);
            }

        }
    }
}
