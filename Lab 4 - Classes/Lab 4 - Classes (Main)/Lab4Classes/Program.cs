﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            RegularRobot regRobot = new RegularRobot(0, "WALL-E", "Blue", 9, 11, RegularRobot.Esize.Medium);

            AdminRobot admRobot = new AdminRobot();
            admRobot.SpecialCommand(0);
             
            Console.WriteLine("");
            Console.WriteLine(RegularRobot.Esize.Small & RegularRobot.Esize.Medium);
            Console.WriteLine(RegularRobot.Esize.Small ^ RegularRobot.Esize.Medium);

            int countClearing = 0;

            clearGameLog(ref countClearing);

            int twoLVL;
            addTwoLevels(25,26, out twoLVL);

            Console.WriteLine("Amount of 2 players LVLs - " + twoLVL);



            Console.WriteLine("----------------------------------------------------------------------");


            Object obj = new object();
            int a = obj.GetHashCode();
            Type b = obj.GetType();
            Console.WriteLine(a + "\n" + b);
            







            Console.ReadLine();
        }

        static void clearGameLog(ref int x)
        {
            x++;
            Console.WriteLine("GameLog was cleared " + x + " times.");
        }

        static void addTwoLevels(int a, int b, out int twoLVL)
        {
            twoLVL = a + b;
        }
    }
}
