﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab4Classes
{
    class RegularRobot : minRobot, Actions
    {
        public enum Esize
        {
            Small = 1, Medium =2, Large =4, EXTRALarge=8
        }

        int rank { get; set; }
        private int countOfSuccessLVL { get; set; }
        private Esize size;

        public RegularRobot(int id, string name, string color, int rank, int countOfSuccessLVL)
        {
            this.id = id;
            this.name = name;
            this.color = color;
            this.rank = rank;
            this.countOfSuccessLVL = countOfSuccessLVL;
        }

        public RegularRobot(int id, string name, string color, int rank, int countOfSuccessLVL, Esize size)
        {
            this.id = id;
            this.name = name;
            this.color = color;
            this.rank = rank;
            this.countOfSuccessLVL = countOfSuccessLVL; //WTF moment
            this.size = size;
        }

        public void Move(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot did " + i + "-th step on move up.");
            }
        }

        public void TurnLeft(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot did " + i + "-th step turn left.");
                if (i%4 == 0)
                {
                    Console.WriteLine("Robot return's to same position.");
                }
            }
        }

        public void TurnRight(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot did " + i + "-th step turn right.");
                if (i % 4 == 0)
                {
                    Console.WriteLine("Robot return's to same position.");
                }
            }
        }

        public void Jump(int steps)
        {
            if (steps < 2)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Robot just did jump. That was cool! U can try again.");
            }
            else
            {
                for (int i = 1; i <= steps; i++)
                {
                    if (i < 2)
                    {
                        Thread.Sleep(1000);
                        Console.WriteLine("Robot just did jump. That was cool! U can try again.");
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        Thread.Sleep(1000);
                        Console.WriteLine("Robot did another jump. Its was'nt so cool, like in first time...");
                    }
                }
            }
        }

        public void Light(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                if (i % 2 != 0)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Light is On!");
                }
                else
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Light is Off!");
                }
            }
        }

        public void SpecialCommand(int id)
        {
            Console.WriteLine("Friend request to Robot with ID - !" + id);
        }

        protected override void log()
        {
            Console.WriteLine("Printing message to log-console...");
        }
    }
}
