﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Classes
{
    abstract class minRobot
    {
        protected int id { get; set; }
        protected string name { get; set; }
        protected string color { get; set; }

        protected abstract void log();
    }
}
