﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!\n\n");

            Robot LightBot = new Robot();

            LightBot.Moving += LightBot_Moving;

            //LightBot.MovingUp = Druk;
            //LightBot.MovingUp = Druk;

            LightBot.MoveUp(5);
            LightBot.TurnLeft(1);
            LightBot.MoveUp(2);
            LightBot.TurnRight(4);
            LightBot.Jump(1);
            LightBot.Light(10);



            Console.ReadLine();
        }

        private static void LightBot_Moving(object sender, MovingEventArgs e)
        {
            Console.WriteLine(e.Text);
        }


        //static void Testov(int aa, string bb)
        //{
        //    Console.WriteLine("FirstFunction - " + bb);
        //}
        //static void Pestov(int aa, string bb)
        //{
        //    Console.WriteLine("SecondFunction - " + bb);
        //}
    }
}
