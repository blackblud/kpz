﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Laba2Events
{
    class Robot
    {
        public delegate void MyDelegate(string text);

        public void MoveUp(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);

                if(Moving != null)
                Moving(this, new MovingEventArgs(string.Format("Robot did " + i + "-th step on move up.")));
            }
        }

        public void TurnLeft(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);

                if (Moving != null)
                    Moving(this, new MovingEventArgs(string.Format("Robot did " + i + "-th turn left.")));
                if (i == 4)
                {
                    Console.WriteLine("Robot return's to same position.");
                }
            }
        }

        public void TurnRight(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                Thread.Sleep(1000);

                if (Moving != null)
                    Moving(this, new MovingEventArgs(string.Format("Robot did " + i + "-th turn right.")));
                if (i == 4)
                {
                    Console.WriteLine("Robot return's to same position.");
                }
            }
        }

        public void Jump(int steps)
        {
            if (steps < 2)
            {
                Thread.Sleep(1000);

                if (Moving != null)
                    Moving(this, new MovingEventArgs(string.Format("Robot just did jump. That was cool! U can try again.")));
            }
            else
            {
                for (int i = 1; i <= steps; i++)
                {
                    if (i<2)
                    {
                        Thread.Sleep(1000);

                        if (Moving != null)
                            Moving(this,
                                new MovingEventArgs(string.Format("Robot just did jump. That was cool! U can try again.")));
                    }
                    else
                    {
                        Thread.Sleep(1000);

                        if (Moving != null)
                            Moving(this,
                                new MovingEventArgs(
                                    string.Format("Robot did another jump. Its was'nt so cool, like in first time...")));
                    }
                }
            }
        }

        public void Light(int steps)
        {
            for (int i = 1; i <= steps; i++)
            {
                if (i % 2 != 0)
                {
                    Thread.Sleep(1000);

                    if (Moving != null)
                        Moving(this, new MovingEventArgs(string.Format("Light is On!")));
                }
                else
                {
                    Thread.Sleep(1000);

                    if (Moving != null)
                        Moving(this, new MovingEventArgs(string.Format("Light is Off!")));
                }
                
                
            }
        }

        public event EventHandler<MovingEventArgs> Moving;
    }
}
