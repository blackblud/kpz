﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2Events
{
    class MovingEventArgs : EventArgs
    {
        public MovingEventArgs(string text)
        {
            Text = text;
        }

        public string Text { get; private set; }
    }
}
