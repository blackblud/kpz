﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Configuration;

namespace Laba6_1ADO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string connectionString;
        SqlDataAdapter adapter;
        DataTable emploTable;

        public MainWindow()
        {
            InitializeComponent();

            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        private void UpdateButtonClick(object sender, RoutedEventArgs e)
        {
            UpdateDB();
        }

        private void DeleteButtonClick(object sender, RoutedEventArgs e)
        {
            if (employeesGrid.SelectedItems != null)
            {
                for (int i = 0; i < employeesGrid.SelectedItems.Count; i++)
                {
                    DataRowView datarowView = employeesGrid.SelectedItems[i] as DataRowView;
                    if (datarowView != null)
                    {
                        DataRow dataRow = (DataRow)datarowView.Row;
                        dataRow.Delete();
                    }
                }
            }
            UpdateDB();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string sql = "SELECT * FROM Emplo";
            emploTable = new DataTable();
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, connection);
                adapter = new SqlDataAdapter(command);

                adapter.InsertCommand = new SqlCommand("InsertEmplo", connection);
                adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@FName", SqlDbType.VarChar, 20, "FirstName"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@LName", SqlDbType.VarChar, 20, "LastName"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@BDate", SqlDbType.Date, 0, "BirthDate"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@adress", SqlDbType.VarChar, 25, "Adress"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@phone", SqlDbType.VarChar, 12, "PhoneNumber"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 35, "Email"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@passcode", SqlDbType.VarChar, 9, "PassCode"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@idcode", SqlDbType.VarChar, 10, "IdentCode"));

                SqlParameter parameter = adapter.InsertCommand.Parameters.Add("@Id", SqlDbType.Int, 0, "Id");
                parameter.Direction = ParameterDirection.Output;

                connection.Open();
                adapter.Fill(emploTable);
                employeesGrid.ItemsSource = emploTable.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        private void UpdateDB()
        {
            SqlCommandBuilder comandbuilder = new SqlCommandBuilder(adapter);
            adapter.Update(emploTable);
        }
    }
}
