CREATE or ALTER PROCEDURE [dbo].[InsertEmplo]
    @FName varchar(20),
    @LName varchar(20),
	@BDate date,
	@adress varchar(25),
	@phone varchar(12),
	@email varchar(35),
	@passcode varchar(9),
	@idcode varchar(10),

    @Id int out
AS
    INSERT INTO Emplo (FirstName, LastName, BirthDate, Adress, PhoneNumber, Email, PassCode, IdentCode)
    VALUES (@FName, @LName, @BDate, @adress, @phone, @email, @passcode, @idcode)
   
    SET @Id=SCOPE_IDENTITY()
GO