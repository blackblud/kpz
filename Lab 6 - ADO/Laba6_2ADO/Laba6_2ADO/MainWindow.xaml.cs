﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Laba6_2ADO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new ViewModel();
        }

        private void UpdateButtonClick(object sender, RoutedEventArgs e)
        {
            ((ViewModel)DataContext).Save();
        }

        private void DeleteButtonClick(object sender, RoutedEventArgs e)
        {
            if (employeesGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < employeesGrid.SelectedItems.Count; i++)
                {
                    Emplo employee = employeesGrid.SelectedItems[i] as Emplo;
                    if (employee != null)
                    {
                        ((ViewModel)DataContext).employees.Remove(employee);
                    }
                }
            }
            ((ViewModel)DataContext).Save();
        }
    }
}
