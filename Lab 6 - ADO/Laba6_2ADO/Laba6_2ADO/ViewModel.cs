﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba6_2ADO
{
    class ViewModel
    {
        KPZ_DBEntities KPZ_DBEntities;

        public ObservableCollection<Emplo> employees { get; set; }

        public ViewModel()
        {
            KPZ_DBEntities = new KPZ_DBEntities();
            KPZ_DBEntities.Emploes.Load();
            employees = KPZ_DBEntities.Emploes.Local;

        }

        public void Save()
        {
            KPZ_DBEntities.SaveChanges();
        }
    }
}
