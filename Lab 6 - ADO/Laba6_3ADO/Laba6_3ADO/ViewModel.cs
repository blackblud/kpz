﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laba6_3ADO.Models;

namespace Laba6_3ADO
{
    class ViewModel
    {
        Context context;

        public ObservableCollection<Employer> employees { get; set; }

        public ViewModel()
        {
            context = new Context();
            context.Employees.Load();
            employees = context.Employees.Local;

        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
