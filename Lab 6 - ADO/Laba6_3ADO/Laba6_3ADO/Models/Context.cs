﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Laba6_3ADO.Models
{
    class Context:DbContext
    {
        public Context() : base("DefaultConnection"){}
        public DbSet<Employer> Employees { get; set; }
    }
}
